---
slug: "host-a-static-website-on-aws"
identifier: "host-a-static-website-on-aws-challenge"
type: "challenge"
parent: ""
lang: "en"
title: "Host a static website on AWS"
date: "2021-01-16"
tags: ["aws", "react", "javascript"]
published: true
description: "Comment héberger un site statique sur AWS"
level: "1"
author: "Thibault"
cover_image: "../images/logo_transparent.png"
---

Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras lobortis ornare posuere. Integer at velit ultrices, sollicitudin mauris non, fringilla justo. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras lobortis ornare posuere. Integer at velit ultrices, sollicitudin mauris non, fringilla justo. 

Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras lobortis ornare posuere. Integer at velit ultrices, sollicitudin mauris non, fringilla justo. 

<!-- end -->

Awesome article

![Alt text here](../images/logo_transparent.png)

Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras lobortis ornare posuere. Integer at velit ultrices, sollicitudin mauris non, fringilla justo. Vivamus augue erat, rhoncus ac dictum sit amet, auctor at velit. Vestibulum ut malesuada velit. In magna turpis, scelerisque maximus magna et, ultrices blandit quam. Maecenas tristique dapibus nisi in volutpat. Quisque nec tincidunt diam. Etiam in tortor tempor dolor tristique vulputate. Proin dolor ipsum, vestibulum quis purus ut, dapibus porta sapien. Vivamus fermentum consectetur quam, ut molestie risus dignissim non. Fusce quam tellus, hendrerit ullamcorper massa non, volutpat aliquet neque. Nullam vitae dolor gravida, lacinia ipsum nec, blandit tortor. Morbi ligula nunc, suscipit nec ipsum non, tempus iaculis odio. Sed commodo vulputate diam ac molestie.

Maecenas nec suscipit metus, nec ornare ante. Morbi sagittis scelerisque purus, sit amet finibus metus. In laoreet augue sit amet est consectetur vehicula. Maecenas in turpis pulvinar, elementum tellus sit amet, condimentum ipsum. Fusce rutrum, felis non volutpat semper, nibh lacus posuere justo, at vehicula libero ante a magna. Aliquam fermentum nibh tortor, at tincidunt lacus finibus quis. Sed mattis sapien non metus lobortis mollis. Ut quis aliquet mauris. Nunc suscipit nunc non justo laoreet rhoncus. Proin auctor massa ut tellus dignissim consequat. Ut interdum id risus eu gravida. In eleifend blandit urna nec aliquam. Nam nec massa porta, fermentum elit ut, gravida tortor. Sed sed viverra nisl, vitae viverra dui. Duis convallis neque id orci porttitor, eget luctus odio varius.

Sed id eros nec leo varius commodo quis id sapien. Aliquam erat volutpat. Suspendisse vel maximus odio, a ornare nulla. Nulla ac nisl suscipit, suscipit nulla et, viverra libero. Vivamus nec sem sit amet lacus fermentum egestas at sit amet massa. Praesent non pharetra eros. Aliquam vitae pretium massa. Curabitur faucibus consectetur mauris, a ullamcorper sapien commodo eget. Vestibulum vehicula nisi a libero sagittis, vitae suscipit dolor porta. Phasellus lacus mi, euismod in neque volutpat, pharetra mattis ligula. Sed suscipit pellentesque neque ac consequat. Nunc sagittis odio lectus, eu venenatis nibh aliquet vel. Donec malesuada posuere odio, et varius nulla imperdiet id. Aenean euismod est leo, nec congue ex aliquet vitae.

Aenean vel consequat nibh, a pharetra sem. Duis pharetra diam laoreet dictum commodo. Fusce mattis dignissim mauris, ac vestibulum dolor faucibus varius. Pellentesque sit amet nunc ornare, aliquet tortor in, mattis elit. Proin non lectus at urna tristique gravida sit amet eu lacus. Nullam dignissim libero in egestas rhoncus. Cras suscipit sed nibh rutrum volutpat. Donec enim nisl, interdum ut eros vel, ornare pretium odio. Ut nisi lorem, tincidunt egestas aliquam vitae, luctus venenatis enim. Proin tempor quam vitae feugiat sollicitudin. Curabitur vel arcu semper, ultricies ante at, tristique leo.

Quisque nec nunc nec nunc dignissim rhoncus non sit amet velit. Fusce euismod lacinia ipsum a malesuada. Maecenas lacinia ut diam quis luctus. Donec eu dapibus lacus. Duis blandit convallis tellus ac vulputate. Sed quis diam lectus. Duis iaculis mi enim.