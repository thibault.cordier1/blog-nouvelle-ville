import * as React from "react"
import { graphql } from 'gatsby'
import Layout from "../components/layout";
import SEO from "../components/seo";





const TeamPage = ({ pageContext, data }) => {


  // TODO: Replace with Markdown frontmatter fields
  const getPersonaDescription = (name) => {
    switch (name) {
      case 'patricia':
        return {
          'name': 'Patrica',
          'role': 'CEO'
        }
      case 'sebastian':
        return {
          'name': 'Sebastian',
          'role': 'CTO'
        }
      default:
        return {
          'name': name,
          'role': 'Unknown'
        }
    }
  }


  // https://tailwindcomponents.com/component/profile-card-2
  const members = data.team.nodes.map((avatar, index) => {
    const { fluid } = avatar.childImageSharp;
    return (
      <div key={"team-member-" + index} className="max-w-xs">
        <div className="bg-white shadow-xl rounded-lg px-3">
          <div className="photo-wrapper p-2">
            <img className="w-32 h-32 rounded-full mx-auto" src={fluid.src} alt={avatar.name} />
          </div>
          <div className="p-2">
            <h3 className="text-center text-xl text-gray-900 font-medium leading-8">{avatar.name}</h3>
            <div className="text-center text-gray-400 text-xs font-semibold">
              <p>{getPersonaDescription(avatar.name).role}</p>
            </div>

          </div>
        </div>
      </div>
    )
  })

  return (
    <Layout>
      <SEO title={"The Team"} />
      <div className="container mx-auto">
        <p className="text-4xl">The Team</p>
        <div className="flex mt-4 flex-col md:flex-row lg:flex-row md:flex-wrap  lg:flex-wrap items-center w-full justify-center lg:space-x-4">
          {members}
        </div>
      </div>

    </Layout>
  )
}

export default TeamPage

export const query = graphql`
query {
  site: site {
    siteMetadata {
      title
    }
  }
  team: allFile(filter: {relativePath: {regex: "/personas/"}}) {
    nodes {
      relativePath
      name
      extension
      childImageSharp {
        fluid(maxWidth: 300) {
            ...GatsbyImageSharpFluid
        }
      }
    }
  }
}
`