import * as React from "react"
import { graphql } from 'gatsby'
import Layout from "../components/layout";
import SEO from "../components/seo";
import BlogRoot from "../components/blog_root";

// markup
const BlogPage = ({ data }) => {

  const posts = data.posts.nodes;
  
  return (
    <Layout>
      <div className="flex flex-wrap md:flex-wrap lg:flex-wrap mx-auto w-full justify-between">
       <BlogRoot posts={posts}/>
      </div>
      <SEO title={"Welcome"} />
    </Layout>
  )
}

export default BlogPage

export const query = graphql`
query {
  site: site {
    siteMetadata {
      title
    }
  },
  posts: allMarkdownRemark(filter: {frontmatter: {type: {in: ["challenge", "story"]}}}) {
    nodes {
      id
      frontmatter {
        cover_image {
          childImageSharp {
              fluid(maxWidth: 300) {
                ...GatsbyImageSharpFluid
              }
            }
        }
        author
        tags
        date
        description
        title
        slug
        published
        type
        lang
        identifier
      }
      html
      wordCount {
        words
      }
      fields {
        readingTime {
          minutes
        }
      }
      excerpt
    }
  }
}
`