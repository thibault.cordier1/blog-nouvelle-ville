import * as React from "react"
import { graphql } from 'gatsby'
import Layout from "../components/layout";
import SEO from "../components/seo";
import Homepage from "../components/homepage";

// markup
const IndexPage = ({ data }) => {

 return (
    <Layout>
      
      <Homepage data={data} />
      <SEO title={"Welcome"} />
    </Layout>
  )
}

export default IndexPage

export const query = graphql`
query {
  site: site {
    siteMetadata {
      title
    }
  },
  slack_join: allMarkdownRemark(filter: {frontmatter: {role: {eq: "slack_join"}}} ) {
    nodes {
    id
    frontmatter {
      cover_image {
        childImageSharp {
            fluid(maxWidth: 300) {
              ...GatsbyImageSharpFluid
            }
          }
      }
      date
      description
      title
      slug
      published
      type
      lang
    }
    html
    wordCount {
      words
    }
  }
}
  welcome_block: allMarkdownRemark(filter: {frontmatter: {role: {eq: "welcome_message"}}} ) {
      nodes {
      id
      frontmatter {
        cover_image {
          childImageSharp {
              fluid(maxWidth: 300) {
                ...GatsbyImageSharpFluid
              }
            }
        }
        date
        description
        title
        slug
        published
        type
        lang
      }
      html
      wordCount {
        words
      }
    }
  }
}
`