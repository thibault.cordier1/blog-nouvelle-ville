import React from "react"
import { graphql } from "gatsby"
import Layout from "../components/layout";
import AlternateSelector from "../components/alternate_selector";
import TagList from "../components/tag_list";
import SEO from "../components/seo";
import PostListItem from "../components/post_list_item";
import DateLabel from "../components/date_label";
import AuthorLabel from "../components/author_label";
import TranslationSelector from "../components/translation_selector";

export default function BlogTemplate({ pageContext, data }) {
    const { post, parent, translations } = data;
    const { html, description } = post;
    const { title, tags, type, date, author, identifier } = post.frontmatter;

    let parent_render = "";
    if (type === "solution") {
        parent_render = <PostListItem embed={true} post={parent} />
    }

    return (
        <Layout>
            <SEO description={description} title={title} />

            <div className="bg-white md:mt-2 lg:mt-2 h-full p-2">
                <div className="float-right mt-4 mr-0 lg:mr-2 border-2">
                    <TranslationSelector translations={translations.nodes} />
                </div>
                <h2 className="text-4xl">{title}</h2>

                <TagList tags={tags} />
                <DateLabel>{date}</DateLabel>
                <AuthorLabel>{author}</AuthorLabel>
                {parent_render}

                {(type === 'challenge' || type === 'solution') &&
                    <React.Fragment>
                        <AlternateSelector type={type} options={data.alternates} current_slug={pageContext.slug} current_identifier={identifier} />
                    </React.Fragment>

                }
                <div
                    className="blog-post-content markdown-html mt-4"
                    dangerouslySetInnerHTML={{ __html: html }}
                />
            </div>


        </Layout>
    );
};


export const pageQuery = graphql`
    query($id: String!, $slug: String!, $parent: String, $identifier: String) {
        post:   markdownRemark(frontmatter: {slug: {eq: $slug } }) {
                id
                html
                frontmatter {
                    author
                    tags
                    slug
                    tags
                    description
                    title
                    published
                    type
                    lang
                    identifier
                    date(
                        formatString: "D MMMM YYYY"
                        locale: "fr-FR"
                    )

                }
            }
            parent:
                markdownRemark(
                    frontmatter: {identifier: {eq: $parent } }
                ) {

                    id
                    html
                    excerpt
                    frontmatter {


                        cover_image {
                            childImageSharp {
                                fluid(maxWidth: 300) {
                                    ...GatsbyImageSharpFluid
                                }
                            }
                        }
                        type
                        tags
                        slug
                        tags
                        description
                        title
                        published
                        author
                        date
                        lang
                        identifier
                    }
                    
                }
            alternates:
                allMarkdownRemark(
                    filter: {frontmatter: {parent: {eq: $parent }, identifier: {ne: $identifier } } }
                ) {
                    nodes {
                        id
                        html
                        excerpt
                        frontmatter {
                            cover_image {
                                childImageSharp {
                                    fluid(maxWidth: 500) {
                                    ...GatsbyImageSharpFluid
                                    }
                                }
                            }              
                        type
                        tags
                        slug
                        tags
                        description
                        title
                        published
                        lang
                        identifier
                        }
                    }
              
                }
        translations:
            allMarkdownRemark(
                filter: {
                    id : { ne: $id },
                    frontmatter: {
                        identifier: {eq: $identifier }
                    }
                }
            ) {
                nodes {
                    id
                    frontmatter {
                    type
                    slug
                    title
                    published
                    lang
                    identifier
                    }
              
                }            
            }


  }
`
