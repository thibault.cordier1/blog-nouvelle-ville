import React from "react"
import { graphql } from "gatsby"
import PostListItem from "../components/post_list_item";
import Layout from "../components/layout";


export default function tagTemplate({pageContext, data}) {
    const { tag } = pageContext;
    const posts  = data.allMarkdownRemark.nodes;
    const postsRender = posts.map((post, index) => {
        return (
            <PostListItem key={"post-" + index} post={post}/>
        )
    })
    return (
        <Layout>

            <div className="container mx-auto">
            <h1 className="text-4xl text-bold">Tag {tag}</h1>
            
            <div className="space-y-4">
            {postsRender}
            </div>

            </div>
        </Layout>
    )
}

export const pageQuery = graphql`
    query($tag: String!) {
      allMarkdownRemark(
          filter: {frontmatter: { tags: { eq: $tag } } }
          ) {
        nodes {
            id
            html
            frontmatter {
                cover_image {
                    childImageSharp {
                        fluid(maxWidth: 300) {
                          ...GatsbyImageSharpFluid
                        }
                      }
                }
                author
                date
                tags
                slug
                tags
                description
                title
                published
                lang
                identifier
            }
            excerpt(pruneLength: 300)
        }
       
    }
  }
`
