import { graphql, Link, useStaticQuery } from "gatsby"
import * as React from "react"
import Img from "gatsby-image"
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome"
import LocaleSelector from "./locale_selector"

export default function Navbar() {
    const data = useStaticQuery(graphql`
    query SiteTitleQuery {
      site: site {
        siteMetadata {
          title
        }
      }
      logo: file(name: {eq: "logo_transparent"})  {
        childImageSharp {
          # Specify the image processing specifications right in the query.
          # Makes it trivial to update as your page's design changes.
          fluid(maxWidth: 700) {
            ...GatsbyImageSharpFluid_noBase64
          }
        }
    }
    }
  `)
    const { title } = data.site.siteMetadata;
    const [navbarOpen, setNavbarOpen] = React.useState(false);
    return (
        <header id="top" className="z-40 w-full lg:container lg:mx-auto sm:relative bg-gray-900 sm:mb-0 md:mb-0 lg:mb-0  lg:border-b-4 lg:border-teal-500">
            <nav id="site-menu" className="flex flex-wrap sm:flex-row  justify-between items-center px-4 sm:px-6 py-1 bg-grey-900 shadow sm:shadow-none">
                <div className="w-full md:w-auto lg:w-auto md:self-center flex flex-row">
                    <Link to="/" className="no-underline py-1 ">
                        <Img className="float-left h-12 w-12" fluid={data.logo.childImageSharp.fluid} />
                    </Link>
                    <Link to="/" className="no-underline ml-2 py-1 ">
                        <h1 className="font-bold text-lg">{title}</h1>
                    </Link>
                    <div className="flex flex-row flex-reverse ml-auto">
                        <div className="block md:hidden lg:hidden">
                            <LocaleSelector />
                        </div>
                        <button
                            className="text-white inline-flex p-3 hover:bg-gray-900 rounded lg:hidden ml-auto hover:text-white outline-none nav-toggler"
                            data-target="#navigation" onClick={() => setNavbarOpen(!navbarOpen)}
                        >
                            <FontAwesomeIcon icon={["fas", "bars"]} />
                        </button>
                    </div>
                </div>

                <div
                    className={
                        "lg:flex flex-grow items-center" +
                        (navbarOpen ? " flex" : " hidden")
                    }
                    id="navigation"
                >
                    <div
                        className="lg:inline-flex lg:flex-row lg:ml-auto lg:w-auto w-full lg:items-center items-start flex flex-wrap md:flex-row lg:flex-row lg:h-auto"
                    >
                        <div className="hidden sm:hidden md:block lg:block sm:mr-4">
                            <LocaleSelector />
                        </div>
                        <Link to="/blog/" className="text-dark font-bold hover:text-red text-lg w-auto no-underline mr-2 sm:pr-4 py-2 sm:py-1 sm:pt-2">Blog</Link>
                        <Link to="/team/" className="text-dark font-bold hover:text-red text-lg w-auto no-underline sm:px-4 py-2 sm:py-1 sm:pt-2">The Team</Link>

                    </div>
                </div>
            </nav>
        </header >
    );
}