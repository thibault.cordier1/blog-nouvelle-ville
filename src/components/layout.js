import * as React from "react"
import '../css/layout.css'
import { library } from '@fortawesome/fontawesome-svg-core'
import { faCalendar, faUser, faTag, faHome, faBars, faLanguage } from '@fortawesome/free-solid-svg-icons'



import { LocaleProvider } from "../contexts/locale"
import Navbar from "./navbar"


library.add(faCalendar)
library.add(faUser)
library.add(faTag)
library.add(faHome)
library.add(faBars)
library.add(faLanguage)

export default function Layout({ children }) {
  return (
    <LocaleProvider>
    <div className="text-gray-100 bg-gray-100 w-full min-h-screen">
      <Navbar/>
      <div className="pt-1 md:pt-1 lg:pt-1 bg-gray-100 min-h-full text-gray-900 h-100 md:mx-auto lg:-mx-auto md:container lg:container md:p-0 lg:p-0">
        <main className="min-h-full">{children}</main>
      </div>

    </div>
    </LocaleProvider>
  )

}
