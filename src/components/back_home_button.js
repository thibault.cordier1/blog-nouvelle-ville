import { Link } from "gatsby";
import * as React from "react"
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

export default function BackHomeButton() {

    return (
        <Link to="/" className="border border-indigo-500 bg-indigo-500 text-white rounded-md px-4 py-2 m-2 transition duration-500 ease select-none hover:bg-indigo-600 focus:outline-none focus:shadow-outline"><FontAwesomeIcon icon={["fas", "home"]} /> Return to Home</Link>
    )

}