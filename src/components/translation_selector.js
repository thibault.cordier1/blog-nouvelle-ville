import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { Link } from "gatsby";
import * as React from "react";
import Flag from 'react-world-flags'

const _ = require("lodash")



export default function TranslationSelector(props) {
    const { translations } = props;

    if (translations.length === 0) {
        return '';
    } 

    const langMap = {
        en: 'USA',
        fr: 'FRA',
        es: 'ESP',
        ch: 'CHN'
    }

    let alternatives = translations.map((node, index) => {
        return (
            <React.Fragment key={index}>
                <div className="h-4 w-8 m-1 mt-2">
                    <Link to={"/posts/" + node.frontmatter.lang + "/" + _.kebabCase(node.frontmatter.slug)} >
                        <Flag key={index} height="16" code={langMap[node.frontmatter.lang]} />
                    </Link>
                </div>
            </React.Fragment>
        )
    })

    return (
        <div className="bg-gray-100 p-0">
            <div className="border-b-2 p-0">
                <span className="text-4xl text-blue-900"><FontAwesomeIcon icon={["fas", "language"]} /></span>
            </div>
            <div className="p-1 bg-white">
                {alternatives}
            </div>
        </div>
    )
}