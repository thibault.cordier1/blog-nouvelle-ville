
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { Link } from "gatsby";
import * as React from "react";


export default function TagList(props) {

    const tagsRender = props.tags.map((tag, index) => {

        return (
            <Link key={props.slug + "_tag_link_" + index} to={"/tags/" + tag}>
                <span className="mt-2 ml-1 inline-block rounded text-white 
        bg-green-400 hover:bg-green-500 duration-300 
        text-xs 
        mr-1 md:mr-2 mb-1 px-1 md:px-4 py-1 
        opacity-90 hover:opacity-100" key={props.slug + "_tag_badge_" + index} variant="primary"><FontAwesomeIcon icon={["fas", "tag"]} /> {tag}</span>
            </Link>
        )
    })


    return <React.Fragment>
        {tagsRender}
    </React.Fragment>;
}