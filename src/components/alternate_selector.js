import * as React from "react"
import { Link } from "gatsby"
import { useLocale } from "../contexts/locale";
const _ = require("lodash")

export default function AlternateSelector(props) {

    const { options } = props;
    const { getLocalizedSolutions } = useLocale();

    const localized_solutions = getLocalizedSolutions(options.nodes);

    const options_list = localized_solutions.map((option, index) => {
        const { id } = option;
        const { title, slug, cover_image, lang } = option.frontmatter;
        return (

            <div key={id} className="box-border w-full lg:w-48 h-32 w-24 px-2 py-2  lg:max-w-xs  h-full">
                <Link to={"/posts/" + lang + "/" + _.kebabCase(slug)}>
                    <div className="bg-white shadow-2xl rounded-lg mb-6 tracking-wide" >
                        <div className="flex-shrink-0 object-contain">
                            <img  src={cover_image.childImageSharp.fluid.src} alt="" className="w-full rounded-lg rounded-b-none" />
                        </div>
                        <div className="px-4 border h-20 py-2">
                            <h2 className="font-bold text-2xl text-gray-800 tracking-normal">{title}</h2>
                        </div>
                    </div>
                </Link>
            </div >

        )
    })

    return (
        <React.Fragment>
                <div className="text-left">

                    <div className="bg-gray-800 text-2xl max-w-max text-white p-2 rounded  leading-none flex items-center">
                        Solutions
                        <span className="bg-white p-1 font-bold rounded text-yellow-600 text-xs ml-2">
                            {localized_solutions.length}
                            </span>
                        </div>
                    <div>
                        <div className="flex flex-wrap pt-1">
                            {options_list}
                        </div>
                        <div className="text-right">
                            <span className="font-bold text-red-400">Propose an alternate method ?</span>
                        </div>
                    </div>
                </div>


        </React.Fragment>

    );
}