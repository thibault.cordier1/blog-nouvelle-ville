import * as React from "react"
import { useLocale } from "../contexts/locale";
import PostListItem from "./post_list_item";

export default function BlogRoot(data) {
  const { posts } = data;
  const { getLocalizedSolutions } = useLocale();
  const localized_posts = getLocalizedSolutions(posts);
  const posts_render = localized_posts.map((post, index) => {
    return (
      <PostListItem key={"post-item" + index} post={post} />
    )
  })
  return (
    <React.Fragment>
      {posts_render}
    </React.Fragment>
  )
}