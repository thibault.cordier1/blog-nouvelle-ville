import { useStaticQuery, graphql } from "gatsby";
import { Helmet } from "react-helmet";
import * as React from "react"

export default function SEO({ description, lang = 'en', title }) {
    const { site } = useStaticQuery(
        graphql`
        query {
            site {
                siteMetadata {
                title
                description
                }
            }
        }
        `
    )


    const metaDescription = description || site.siteMetadata.description;

    return (
        <Helmet
            htmlAttributes={{ lang }}
            title={title}
            titleTemplate={`%s | ${site.siteMetadata.title}`}

            meta={[
                {
                    name: "description",
                    content: metaDescription
                },
                // Twitter
                {
                    name: `twitter:title`,
                    content: title,
                },
                {
                    name: `twitter:description`,
                    content: metaDescription,
                },
            ]}
        />
    )
}