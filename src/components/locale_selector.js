import * as React from "react"
import { useLocale } from "../contexts/locale";
import ReactFlagsSelect from 'react-flags-select';

export default function LocaleSelector() {
    const { getUserLocale, changeLocale} = useLocale();
    const [selected, setSelected] = React.useState(getUserLocale().toUpperCase());
    const onSelection = ((code) => {
        changeLocale(code.toLowerCase());
        setSelected(code);
    })

    const supported_languages = ["US", "FR"];
    const labels = {
        "US": "English",
        "FR": "Français"
    }

    

        return (
            <React.Fragment>
               <ReactFlagsSelect className="h-9 p-0 bg-gray-800 text-black mt-1 mr-2 md:mr-2 lg:mr-2"
                    selectButtonClassName="rounded bg-gray-100 text-white"
                    selected={selected}
                    countries={supported_languages}
                    onSelect={code => onSelection(code)}
                    customLabels={labels}
                    showOptionLabel={false}
                    showSelectedLabel={false}
                />
            </React.Fragment>
        );

}