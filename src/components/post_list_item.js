import { Link } from "gatsby";
import * as React from "react"
import AuthorLabel from "./author_label";
import DateLabel from "./date_label";
import TagList from "./tag_list";
const _ = require("lodash")

export default function PostListItem(props) {
    const { post, embed } = props;
    const { tags, slug, title, date, author, cover_image, type, lang } = post.frontmatter;



    let cover_image_render = ''
    let cover_image_sm_render = ''
    if (cover_image && cover_image.childImageSharp) {
        cover_image_render = <img alt="" className="object-left rounded-lg w-100" src={cover_image.childImageSharp.fluid.src} />
        cover_image_sm_render = <img alt="" className="object-left float-left rounded-lg mr-2 w-1/3 md:w-32 lg:w-full" src={cover_image.childImageSharp.fluid.src} />
    }



    let post_item_background = "bg-white";
    if(type === "story") {
        post_item_background = "bg-purple-200";
    }

    return (
        <div key={post.id} className="mx-auto w-full mt-10">
            <div className={"border-2 p-2 " + post_item_background}>

                <div className="flex flex-row flex-nowrap">
                    <div className="hidden w-1/6 md:block lg:block">
                        <span className="border-1">
                            <Link to={"/posts/"+lang+"/" + _.kebabCase(slug)}>{cover_image_render}</Link>
                        </span>
                    </div>
                    <div className="md:ml-4 lg:ml-4 w-100  md:w-5/6 lg:w-5/6">
                        <div className="flex flex-row">
                            <Link to={"/posts/"+lang+"/" + _.kebabCase(slug)}><h3 className="text-2xl md:text-4xl
                         lg:test-7xl ml-1 inline-block rounded text-white 
                        bg-gray-500 hover:bg-gray-800 duration-300 
                        text-xs font-bold mr-1 md:mr-2 mb-2 px-2 md:px-4 py-1 pr-2 mr-2">{title}</h3></Link>
                        </div>
                        <div className="flex flex-wrap">
                            {!embed &&
                                <React.Fragment>
                                    <TagList slug={slug} tags={tags} />
                                    <DateLabel>{date}</DateLabel>
                                    <AuthorLabel>{author}</AuthorLabel>
                                </React.Fragment>
                            }


                        </div>
                        <div className="block md:hidden lg:hidden flex flex-row">
                            <div className="inline">
                                <Link to={"/posts/"+lang+"/" + _.kebabCase(slug)}>{cover_image_sm_render}</Link>
                                <span>
                                    {post.excerpt}
                                </span>
                            </div>
                        </div>
                        <div className="mt-4 hidden md:block lg:block inline ml-3 w-auto">
                            <span>
                                {post.excerpt}
                            </span>
                        </div>
                    </div>
                </div>


            </div>

        </div>
    )
}