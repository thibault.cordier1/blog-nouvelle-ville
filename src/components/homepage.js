import * as React from "react"
import { useLocale } from "../contexts/locale";

export default function Homepage(props) {
    const { data } = props;
    const { welcome_block, slack_join } = data;

    const [localized_welcome_block, setLocalizedWelcomeBlock] = React.useState(null);
    const [localized_slack_join, setLocalizedSlackJoinBlock] = React.useState(null);

    const { getLocalizedNode } = useLocale();

    let join_slack_render = '';
    let welcome_block_render = '';

    React.useEffect(() => {
        setLocalizedWelcomeBlock(getLocalizedNode(welcome_block.nodes));
        setLocalizedSlackJoinBlock(getLocalizedNode(slack_join.nodes));
    }, [welcome_block, slack_join, getLocalizedNode])


    if (localized_slack_join) {

        join_slack_render = <div className="card rounded border-2 w-full md:w-3/12 lg:w-7/12 bg-gray-100  shadow-2xl flex-row rounded relative">
            <div className="p-2 bg-gray-200 text-black-900 rounded">{localized_slack_join.frontmatter.title}</div>
            <div className="h-full w-full justify-center items-center flex pb-4">
                <div
                    className="markdown-html p-2 align-middle my-auto w-full"
                    dangerouslySetInnerHTML={{ __html: localized_slack_join.html }}
                />
            </div>
        </div>
    }


    if (localized_welcome_block) {
        welcome_block_render = <div className="card rounded border-2 w-full md:w-3/12 lg:w-4/12 bg-gray-100  shadow-2xl flex-row rounded relative">
            <div className="p-2 bg-gray-200 text-black-900 rounded-t">{localized_welcome_block.frontmatter.title}</div>
            <div className="h-full w-full justify-center items-center flex  pb-4">
                <div
                    className="markdown-html p-2 align-middle my-auto w-full"
                    dangerouslySetInnerHTML={{ __html: localized_welcome_block.html }}
                />
            </div>
        </div>
    }


    return (
        <React.Fragment>
            <div className="flex flex-wrap md:flex-wrap lg:flex-wrap mx-auto justify-between">
                {welcome_block_render}
                {join_slack_render}
            </div>
        </React.Fragment>
    );
}