import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import * as React from "react";

export default function AuthorLabel({children}) {
    return (
        <span className="mt-2 ml-1 inline-block rounded text-white 
        bg-blue-400 hover:bg-blue-500 duration-300 
        text-xs
        mr-1 md:mr-2 mb-2 px-2 md:px-4 py-1 
        opacity-90 hover:opacity-100"><FontAwesomeIcon icon={["fas", "user"]} /> {children}</span>
    )
}