import { createContext, useReducer } from "react";
import * as React from "react"

const isBrowser = typeof window !== 'undefined' && typeof window.document !== 'undefined';

const FAILBACK_LANGUAGE = 'en';
const LOCALE_LOCALSTORAGE_KEY = 'NouvelleVille-locale'

const LocaleContext = createContext();


function saveLocale(locale) {
    if (isBrowser) {
        localStorage.setItem(LOCALE_LOCALSTORAGE_KEY, locale);
    }

}

function getSavedLocale() {
    if (isBrowser) {
        return localStorage.getItem(LOCALE_LOCALSTORAGE_KEY)
    } else {
        return null;
    }

}

function _getBrowserLocales(options = {}) {
    const defaultOptions = {
        languageCodeOnly: false,
    };

    const opt = {
        ...defaultOptions,
        ...options,
    };
    if (isBrowser) {
        const browserLocales =
            navigator.languages === undefined
                ? [navigator.language]
                : navigator.languages;

        if (!browserLocales) {
            return undefined;
        }

        return browserLocales.map(locale => {
            const trimmedLocale = locale.trim();

            return opt.languageCodeOnly
                ? trimmedLocale.split(/-|_/)[0]
                : trimmedLocale;
        });
    } else {
        return undefined;
    }
}

function detectBrowserLocale() {

    const detected_locale = _getBrowserLocales({
        languageCodeOnly: true
    });
    if (!detected_locale) {
        return FAILBACK_LANGUAGE;
    }
    return detected_locale[0];
}

const init = () => {
    let init_context = {
        detected_locale: null,
        user_choosen_locale: null,
        failback_locale: FAILBACK_LANGUAGE,
    }
    init_context['detected_locale'] = detectBrowserLocale();
    init_context['user_choosen_locale'] = getSavedLocale();

    return init_context;
}



function LocaleReducer(state, action) {

    switch (action.type) {
        case 'saveLocale':
            saveLocale(action.locale);
            return {
                ...state,
                user_choosen_locale: action.locale
            };
        default:
            throw new Error(`Unhandled action type: ${action.type}`)
    }
}


function LocaleProvider({ children }) {
    const [state, dispatch] = useReducer(LocaleReducer, {}, init);
    return (
        <LocaleContext.Provider value={{ state, dispatch }}>
            {children}
        </LocaleContext.Provider>
    );
};



function useLocale() {
    const { state, dispatch } = React.useContext(LocaleContext);

    const getUserLocale = () => {
        if (state.user_choosen_locale) {
            return state.user_choosen_locale
        } else {
            if (state.detected_locale) {
                return state.detected_locale;
            } else {
                return state.failback_locale;
            }
        }
    }

    const changeLocale = (locale) => {
        dispatch({ type: 'saveLocale', locale: locale })
    }

    const getLocalizedNode = (node_list) => {
        const hadDetectedLocal = node_list.some((node) => node.frontmatter.lang === getUserLocale());
        const localized_node = (hadDetectedLocal)
            ? node_list.find((node) => node.frontmatter.lang === getUserLocale())
            : node_list.find((node) => node.frontmatter.lang === FAILBACK_LANGUAGE)
        return localized_node
    }

    function groupByIdentifier(objectArray) {
        return objectArray.reduce(function (acc, obj) {
            var key = obj['frontmatter']['identifier'];
            if (!acc[key]) {
                acc[key] = [];
            }
            acc[key].push(obj);
            return acc;
        }, {});
    }

    const getLocalizedSolutions = (node_list) => {
        let sorted_by_identifier = groupByIdentifier(node_list);
        let localized_nodes = Object.keys(sorted_by_identifier).map((key) => {
             let localized_node = getLocalizedNode(sorted_by_identifier[key]);
             if(localized_node) {
                localized_node['translations'] = sorted_by_identifier[key];
                return localized_node;
             }
             return null;
            
        })
        let null_removed = localized_nodes.filter((el) => el != null)
        return null_removed;
    }

    return { state, dispatch, getLocalizedNode, getLocalizedSolutions, getUserLocale, changeLocale }
}

export { LocaleContext, useLocale, LocaleProvider }