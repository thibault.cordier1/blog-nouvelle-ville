
const path = require("path")
const _ = require("lodash")

exports.createPages = async ({ actions, graphql, reporter }) => {
    const { createPage } = actions

    const blogPostTemplate = require.resolve(`./src/templates/blogTemplate.js`)
    const tagTemplate = require.resolve(`./src/templates/tagTemplate.js`)

    const result = await graphql(`
      {
        postsRemark: allMarkdownRemark(
          filter: {frontmatter: { type:  { in: ["challenge","story", "solution"]}}}
          sort: { order: DESC, fields: [frontmatter___date] }
          limit: 1000
        ) {
          edges {
            node {
              id
              frontmatter {
                slug
                parent
                type
                identifier
                lang
              }
            }
          }
        },
        tagsGroup: allMarkdownRemark(limit: 2000) {
            group(field: frontmatter___tags) {
              fieldValue
            }
          }
      }
    `)

    // Handle errors
    if (result.errors) {
        reporter.panicOnBuild(`Error while running GraphQL query.`)
        return
    }

    const posts = result.data.postsRemark.edges

    posts.forEach(({ node }) => {
        let alternate_search = node.frontmatter.parent;
        if (node.frontmatter.type === "challenge") {
          alternate_search = node.frontmatter.identifier;
        }

        createPage({
            path: `/posts/${node.frontmatter.lang}/${_.kebabCase(node.frontmatter.slug)}/`,
            component: blogPostTemplate,
            context: {
                // additional data can be passed via context
                id: node.id,
                slug: node.frontmatter.slug,
                parent: alternate_search,
                identifier: node.frontmatter.identifier
            },
        })
    })

    // Extract tag data from query
    const tags = result.data.tagsGroup.group
    // Make tag pages
    tags.forEach(tag => {
        createPage({
            path: `/tags/${_.kebabCase(tag.fieldValue)}/`,
            component: tagTemplate,
            context: {
                tag: tag.fieldValue,
            },
        })
    })


}