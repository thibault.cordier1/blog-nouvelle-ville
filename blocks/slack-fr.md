---
type: "block"
role: "slack_join"
title: "Rejoignez-nous sur Slack"
date: "2021-01-20"
lang: "fr"
published: true
---
[![Rocket](../images/AWS-SLACK.png)](https://sebs.to/frmeetup)

<center>
<p class="md:text-xl lg:text-xl">Canal:  <span class="text-red-500">#startup-foundation</p></span>
</center>