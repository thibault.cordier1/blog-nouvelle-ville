---
type: "block"
role: "slack_join"
title: "Join us on Slack"
date: "2021-01-20"
lang: "en"
published: true
---
[![Rocket](../images/AWS-SLACK.png)](https://sebs.to/frmeetup)

<center>
<p class="md:text-xl lg:text-xl">Channel:  <span class="text-red-500">#startup-foundation</p></span>
</center>